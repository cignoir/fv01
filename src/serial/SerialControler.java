package serial;

import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;

import java.awt.Toolkit;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.sound.sampled.Clip;

/**
 * For Basic Serial Communication
 * 
 */
public class SerialControler{
	private static final int BAUD_RATE = 9600;
	private static final String COM_PORT = "COM4";
	private static final String LED_STATUS_REQ = "LS\r";
	private static final String LED_STATUS_RES = "LS11";
	private static final int RES_INTERVAL = 5000;
	private static final int REQ_INTERVAL = 500;
	
	SerialPort port;
	CommPortIdentifier comID;

	String comName;
	int baudRate;

	InputStream in;
	OutputStream out;

	long lastExec;
	static String app = null;
	Clip beep;
	
	public static void main(String[] args) {
		if (args.length == 1) {
			app = args[0];
		}
		
		SerialControler serial = new SerialControler();
		serial.open(COM_PORT, BAUD_RATE);
		while (true) {
			serial.write(LED_STATUS_REQ);
			try {
				Thread.sleep(REQ_INTERVAL);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * open the port
	 * 
	 * @param comName
	 * @param baudRate
	 * @return boolean
	 */
	public boolean open(String comName, int baudRate) {
		this.comName = comName;
		this.baudRate = baudRate;

		try {
			comID = CommPortIdentifier.getPortIdentifier(comName);
			port = (SerialPort) comID.open("SerialBase", 2000);
			port.setSerialPortParams(baudRate, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
			port.setFlowControlMode(SerialPort.FLOWCONTROL_NONE);

			port.addEventListener(new SerialPortListener());
			port.notifyOnDataAvailable(true);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Read InputStream
	 * 
	 */
	public void read() {
		try {
			in = port.getInputStream();
			byte[] buffer = new byte[1024];
			int numRead = in.read(buffer);
			if (numRead == -1) {
				return;
			}
			String received = new String(buffer, 0, buffer.length);
			if (received.contains(LED_STATUS_RES)) {
				try {
					if (app != null && System.currentTimeMillis() - lastExec > RES_INTERVAL) {
						Toolkit.getDefaultToolkit().beep();
						Process process = Runtime.getRuntime().exec(app);
						lastExec = System.currentTimeMillis();
						process.waitFor();
						lastExec = System.currentTimeMillis();
					}

				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Write OutputStream
	 * 
	 * @param str
	 */
	void write(String str) {
		try {
			byte[] data = str.getBytes();
			out = port.getOutputStream();
			out.write(data);
			out.flush();
		} catch (Exception e) {
		}
	}

	/**
	 * EventListener
	 * 
	 */
	class SerialPortListener implements SerialPortEventListener {

		public void serialEvent(SerialPortEvent event) {
			if (event.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
				read();
			}
		}
	}
	
}